## Golang Build Container
FROM registry.gitlab.com/strasheim/go-container AS builder

WORKDIR /go/b
COPY . .
ENV GOARCH=amd64 GOOS=linux CGO_ENABLED=0
RUN go build -ldflags "-s -w -extldflags \"-static\""
RUN golangci-lint run

## Final Container
FROM scratch 
COPY --from=builder /go/b/httpserver httpserver
ENTRYPOINT  ["/httpserver"]
